/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sucesiones;

/**
 *
 * @author Jayli
 */
public class hanoi {

    int con = 0;

    public String metodoHanoi(String O, String A, String D, int n) {
        String res = "";

        if (n == 1) {
            con++;
            res += "PASO " + con + " MOVER PLATO " + n + " DEL " + O + " AL " + D + " \n";

        } else {

            res += metodoHanoi(O, D, A, n - 1);
            con++;
            res += "PASO " + con + " MOVER PLATO " + n + " DEL " + O + " AL " + D + " \n";

            res += metodoHanoi(A, O, D, n - 1);
        }

        return res;
    }
}
