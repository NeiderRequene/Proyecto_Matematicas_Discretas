package sucesiones;

import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javafx.beans.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import org.jdesktop.swingx.JXLabel;
import java.awt.Graphics;
import java.util.Calendar;
import org.lsmp.djep.djep.DJep;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;
import java.util.Timer;
import java.util.TimerTask;

public class Frm_Menu1 extends javax.swing.JFrame {

    public Frm_Menu1() {
        initComponents();
        try {
            this.setSize(1200, 700);
            this.setLocationRelativeTo(null);

            Icon icono1 = new ImageIcon(ClassLoader.getSystemResource("Imagenes/blackboard.png"));
            Task_Sucesiones.setIcon(icono1);
            Task_Sucesiones.setTitle("Opciones");
            MenuRegistro1_1();
            MenuRegistro1_2();
            MenuRegistro1_3();
            MenuRegistro1_4();
            Contenedor.setVisible(true);
            Timer timer = new Timer();
            TimerTask tarea = new TimerTask() {
                @Override
                public void run() {
                    calcularFecha();
                }
            };
            timer.schedule(tarea, 1000, 1000);
            //txt_ElegirLimite.setValue("∞");
        } catch (Exception e) {
        }

    }

    public void calcularFecha() {
        Calendar cal = Calendar.getInstance();
        String fecha = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(cal.YEAR);
        String hora = cal.get(cal.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(cal.SECOND);
        this.lblfecha.setText(fecha);
        this.lblhora.setText(hora);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jcMousePanel1 = new jcMousePanel.jcMousePanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jXTaskPaneContainer1 = new org.jdesktop.swingx.JXTaskPaneContainer();
        Task_Sucesiones = new org.jdesktop.swingx.JXTaskPane();
        Task_Conjuntos = new org.jdesktop.swingx.JXTaskPane();
        jLabel33 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        Contenedor = new javax.swing.JPanel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jPanel4 = new javax.swing.JPanel();
        lblhora = new javax.swing.JLabel();
        lblfecha = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(1020, 630));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jcMousePanel1.setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setPreferredSize(new java.awt.Dimension(735, 80));

        jLabel1.setFont(new java.awt.Font("Copperplate Gothic Bold", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Matemáticas Discretas");

        jLabel2.setFont(new java.awt.Font("Copperplate Gothic Bold", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Realizado por: Requené Neider,Jacome Jesus, De la torre Jayli");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(273, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(342, 342, 342))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(226, 226, 226))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jcMousePanel1.add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setBackground(new java.awt.Color(102, 102, 102));
        jPanel2.setPreferredSize(new java.awt.Dimension(200, 300));
        jPanel2.setLayout(new java.awt.BorderLayout());

        org.jdesktop.swingx.VerticalLayout verticalLayout1 = new org.jdesktop.swingx.VerticalLayout();
        verticalLayout1.setGap(14);
        jXTaskPaneContainer1.setLayout(verticalLayout1);

        Task_Sucesiones.setToolTipText("");
        jXTaskPaneContainer1.add(Task_Sucesiones);
        jXTaskPaneContainer1.add(Task_Conjuntos);

        jLabel33.setFont(new java.awt.Font("Cooper Black", 0, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/team2.png"))); // NOI18N
        jLabel33.setText("Bienvenidos");
        jXTaskPaneContainer1.add(jLabel33);

        jPanel2.add(jXTaskPaneContainer1, java.awt.BorderLayout.CENTER);

        jcMousePanel1.add(jPanel2, java.awt.BorderLayout.LINE_START);

        jPanel3.setPreferredSize(new java.awt.Dimension(1120, 550));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        Contenedor.setBackground(new java.awt.Color(117, 230, 243));
        Contenedor.setLayout(new javax.swing.BoxLayout(Contenedor, javax.swing.BoxLayout.LINE_AXIS));

        jDesktopPane1.setAutoscrolls(true);
        Contenedor.add(jDesktopPane1);

        jPanel3.add(Contenedor);

        jcMousePanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(new java.awt.Color(117, 150, 227));

        lblhora.setText("00/00/00");

        lblfecha.setText("00/00/00");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Hora:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Fecha:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(392, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblfecha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblhora)
                .addGap(477, 477, 477))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblfecha)
                    .addComponent(lblhora)
                    .addComponent(jLabel5))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jcMousePanel1.add(jPanel4, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(jcMousePanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public final void MenuRegistro1_1() { //Metodo para agregar las opciones al JXTaskpane
        Icon icono = new ImageIcon(ClassLoader.getSystemResource("Imagenes/cells.png"));
        final JXLabel label = new JXLabel("Sucesiones", icono, JXLabel.LEFT);
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Contenedor.setVisible(true);
                FrmFormulario s = new FrmFormulario();
                jDesktopPane1.add(s);
                s.show();
                s.setLocation(180, 25);

            }
        });
        Task_Sucesiones.add(label);
        eventosMouse(label);
    }

    public final void MenuRegistro1_2() { //Metodo para agregar las opciones al JXTaskpane
        Icon icono = new ImageIcon(ClassLoader.getSystemResource("Imagenes/blackboard.png"));
        final JXLabel label = new JXLabel("Torres de Hannoi", icono, JXLabel.LEFT);
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Contenedor.setVisible(true);
                FrmFormulario1 s1 = new FrmFormulario1();
                jDesktopPane1.add(s1);
                s1.show();
                s1.setLocation(180, 25);

            }
        });
        Task_Sucesiones.add(label);
        eventosMouse(label);
    }

    public final void MenuRegistro1_3() { //Metodo para agregar las opciones al JXTaskpane
        Icon icono = new ImageIcon(ClassLoader.getSystemResource("Imagenes/abacus.png"));
        final JXLabel label = new JXLabel("Conjuntos", icono, JXLabel.LEFT);
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Contenedor.setVisible(true);
                FrmConjuntos s1 = new FrmConjuntos();
                jDesktopPane1.add(s1);
                s1.show();
                s1.setLocation(70, 20);

            }
        });
        Task_Sucesiones.add(label);
        eventosMouse(label);
    }
     public final void MenuRegistro1_4() { //Metodo para agregar las opciones al JXTaskpane
        Icon icono = new ImageIcon(ClassLoader.getSystemResource("Imagenes/blackboard.png"));
        final JXLabel label = new JXLabel("Matrices Booleanas", icono, JXLabel.LEFT);
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Contenedor.setVisible(true);
                Vista s1 = new Vista();
                jDesktopPane1.add(s1);
                s1.show();
                s1.setLocation(70, 20);

            }
        });
        Task_Sucesiones.add(label);
        eventosMouse(label);
    }
    public void eventosMouse(final JXLabel label) {//Metodo para que se pinte 
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent ev) {
                Font font = new Font("Tahoma", Font.BOLD, 13);
                label.setFont(font);
            }
        });

        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent ev) {
                Font font = new Font("Tahoma", Font.PLAIN, 13);
                label.setFont(font);
            }
        });
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
//             UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
            UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel"); //tenia
//             UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.luna.LunaLookAndFeel");
//            UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");
//             UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel"); //MEJOR
//             UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
//            UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");

        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Menu1.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Menu1.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Menu1.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Menu1.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Menu1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Contenedor;
    private org.jdesktop.swingx.JXTaskPane Task_Conjuntos;
    private org.jdesktop.swingx.JXTaskPane Task_Sucesiones;
    public static javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private org.jdesktop.swingx.JXTaskPaneContainer jXTaskPaneContainer1;
    private jcMousePanel.jcMousePanel jcMousePanel1;
    private javax.swing.JLabel lblfecha;
    private javax.swing.JLabel lblhora;
    // End of variables declaration//GEN-END:variables

}
