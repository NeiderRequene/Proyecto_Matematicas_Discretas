/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sucesiones;

/**
 *
 * @author Jayli
 */
public class sucesion_1 {
    
    public double metodo(int numero){
        
        return Math.pow(-1, numero)*(5*numero + 3);
    }
    
    public String numerador(int numero){
        int result =(int)( Math.pow(-1, numero));
        String signo ="";
        if(result== -1){
            signo="-";
        }else{
            signo="+";
        }
        
        int numerador=((6*numero)-2);
        int denominador=(int)Math.pow(2, numero-1);
        String res="";
        res=String.valueOf("     "+numerador+"\n"+signo+"  -----\n"+"     "+denominador);
        
        
        return res;
    }
}
