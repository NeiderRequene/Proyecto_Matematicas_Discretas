package sucesiones;

/**
 *
 * @author Andrés Jácome
 */
public class FuncionesCaracteristicas {

    private String[] U, A, B;
    private int[] fa, fb, fu;
    int nu, na, nb;

    public FuncionesCaracteristicas(String[] U, String[] A, String[] B) {
        this.U = U;
        this.A = A;
        this.B = B;
        nu = U.length;
        na = A.length;
        nb = B.length;
        fu = new int[nu];
        fa = new int[nu];
        fb = new int[nu];
    }

    public void llenarFU() {
        for (int i = 0; i < nu; i++) {
            fu[i] = 1;
        }
    }

    public void llenarFA() {
        for (int i = 0; i < na; i++) {
            for (int j = 0; j < nu; j++) {
                if (A[i].equals(U[j])) {
                    fa[j] = 1;
                    break;
                }
            }
        }
    }

    public void llenarFB() {
        for (int i = 0; i < nb; i++) {
            for (int j = 0; j < nu; j++) {
                if (B[i].equals(U[j])) {
                    fb[j] = 1;
                    break;
                }
            }
        }
    }

    public int[] Interseccion() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] = fa[i] * fb[i];
        }
        return fr;
    }

    public int[] Union() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] = fa[i] + fb[i] - fa[i] * fb[i];
        }
        return fr;
    }

    public int[] RestaA_B() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] = fa[i] - fa[i] * fb[i];
        }
        return fr;
    }

    public int[] RestaB_A() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] =  fb[i] - fa[i] * fb[i];
        }
        return fr;
    }

    public int[] ComplementoA() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] = fu[i] -fa[i];
        }
        return fr;
    }
    
    public int[] ComplementoB() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] = fu[i] -fb[i];
        }
        return fr;
    }

    public int[] DifSimetrica() {
        int[] fr = new int[nu];
        for (int i = 0; i < nu; i++) {
            fr[i] = fa[i] + fb[i] -2*fa[i]*fb[i];
        }
        return fr;
    }
    
    public String resultado(int fr[]) {
        String r = "";
        for (int i = 0; i < nu; i++) {
            if (fr[i] == 1) {
                r +=  U[i] + ",";
            }
        }
        return r;
    }
    
    

}
