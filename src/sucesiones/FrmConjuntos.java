/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sucesiones;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrés Jácome
 */
public class FrmConjuntos extends javax.swing.JInternalFrame {

    /**
     * Creates new form FrmConjuntos
     */
    FuncionesCaracteristicas fc;

    public FrmConjuntos() {
        initComponents();
        txtA.setEnabled(false);
        txtB.setEnabled(false);
        btnIngresar.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtU = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtA = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtB = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btnIngresar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaResultado = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        btnUnion = new javax.swing.JButton();
        btnInterseccion = new javax.swing.JButton();
        btnDifB_A = new javax.swing.JButton();
        btnDif_A_B = new javax.swing.JButton();
        btnComplementoA = new javax.swing.JButton();
        btnDiferenciaSimet = new javax.swing.JButton();
        btnComplementoB = new javax.swing.JButton();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconifiable(true);
        setMaximizable(true);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 51), 5));

        jPanel2.setBackground(new java.awt.Color(0, 102, 102));
        jPanel2.setForeground(new java.awt.Color(0, 102, 102));

        jLabel1.setFont(new java.awt.Font("Sitka Subheading", 1, 40)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 255, 255));
        jLabel1.setText("OPERACIONES CON CONJUNTOS");

        jLabel2.setBackground(new java.awt.Color(204, 255, 255));
        jLabel2.setFont(new java.awt.Font("Sitka Small", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 255, 255));
        jLabel2.setText("REALIZADO POR: JESÚS ANDRÉS JÁCOME");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(122, 122, 122))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(285, 285, 285)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102), 2));

        jLabel3.setBackground(new java.awt.Color(0, 102, 102));
        jLabel3.setFont(new java.awt.Font("Sitka Small", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 102));
        jLabel3.setText("U = {");

        jLabel4.setBackground(new java.awt.Color(0, 102, 102));
        jLabel4.setFont(new java.awt.Font("Sitka Small", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 102));
        jLabel4.setText("}");

        txtU.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        txtU.setForeground(new java.awt.Color(0, 102, 102));
        txtU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUActionPerformed(evt);
            }
        });
        txtU.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUKeyTyped(evt);
            }
        });

        jLabel5.setBackground(new java.awt.Color(0, 102, 102));
        jLabel5.setFont(new java.awt.Font("Sitka Small", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 102));
        jLabel5.setText("A = {");

        txtA.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        txtA.setForeground(new java.awt.Color(0, 102, 102));
        txtA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAActionPerformed(evt);
            }
        });
        txtA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAKeyTyped(evt);
            }
        });

        jLabel6.setBackground(new java.awt.Color(0, 102, 102));
        jLabel6.setFont(new java.awt.Font("Sitka Small", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 102));
        jLabel6.setText("}");

        jLabel7.setBackground(new java.awt.Color(0, 102, 102));
        jLabel7.setFont(new java.awt.Font("Sitka Small", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 102));
        jLabel7.setText("B = {");

        txtB.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        txtB.setForeground(new java.awt.Color(0, 102, 102));
        txtB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBActionPerformed(evt);
            }
        });
        txtB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBKeyTyped(evt);
            }
        });

        jLabel8.setBackground(new java.awt.Color(0, 102, 102));
        jLabel8.setFont(new java.awt.Font("Sitka Small", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 102));
        jLabel8.setText("}");

        btnIngresar.setBackground(new java.awt.Color(204, 255, 255));
        btnIngresar.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnIngresar.setForeground(new java.awt.Color(0, 102, 102));
        btnIngresar.setText("INGRESAR");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtB)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtU)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(44, 44, 44))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 76, Short.MAX_VALUE)
                .addComponent(btnIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(txtU, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(txtA, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(txtB, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnIngresar)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        txaResultado.setColumns(20);
        txaResultado.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        txaResultado.setForeground(new java.awt.Color(0, 102, 102));
        txaResultado.setRows(5);
        txaResultado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102), 2));
        jScrollPane1.setViewportView(txaResultado);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102), 2));

        btnUnion.setBackground(new java.awt.Color(204, 255, 255));
        btnUnion.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnUnion.setForeground(new java.awt.Color(0, 102, 102));
        btnUnion.setText("UNIÓN");
        btnUnion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUnionActionPerformed(evt);
            }
        });

        btnInterseccion.setBackground(new java.awt.Color(204, 255, 255));
        btnInterseccion.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnInterseccion.setForeground(new java.awt.Color(0, 102, 102));
        btnInterseccion.setText("INTERSECCIÓN");
        btnInterseccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInterseccionActionPerformed(evt);
            }
        });

        btnDifB_A.setBackground(new java.awt.Color(204, 255, 255));
        btnDifB_A.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnDifB_A.setForeground(new java.awt.Color(0, 102, 102));
        btnDifB_A.setText("B - A");
        btnDifB_A.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDifB_AActionPerformed(evt);
            }
        });

        btnDif_A_B.setBackground(new java.awt.Color(204, 255, 255));
        btnDif_A_B.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnDif_A_B.setForeground(new java.awt.Color(0, 102, 102));
        btnDif_A_B.setText("A - B");
        btnDif_A_B.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDif_A_BActionPerformed(evt);
            }
        });

        btnComplementoA.setBackground(new java.awt.Color(204, 255, 255));
        btnComplementoA.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnComplementoA.setForeground(new java.awt.Color(0, 102, 102));
        btnComplementoA.setText("COMPLEMENTO A");
        btnComplementoA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComplementoAActionPerformed(evt);
            }
        });

        btnDiferenciaSimet.setBackground(new java.awt.Color(204, 255, 255));
        btnDiferenciaSimet.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnDiferenciaSimet.setForeground(new java.awt.Color(0, 102, 102));
        btnDiferenciaSimet.setText("DIFERENCIA SIMÉTRICA");
        btnDiferenciaSimet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDiferenciaSimetActionPerformed(evt);
            }
        });

        btnComplementoB.setBackground(new java.awt.Color(204, 255, 255));
        btnComplementoB.setFont(new java.awt.Font("Sitka Display", 1, 18)); // NOI18N
        btnComplementoB.setForeground(new java.awt.Color(0, 102, 102));
        btnComplementoB.setText("COMPLEMENTO A");
        btnComplementoB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComplementoBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnDiferenciaSimet)
                        .addGap(18, 18, 18)
                        .addComponent(btnComplementoB)
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnInterseccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnComplementoA, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(btnDif_A_B, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnDifB_A, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnUnion, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInterseccion)
                    .addComponent(btnUnion))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnComplementoA)
                    .addComponent(btnDif_A_B)
                    .addComponent(btnDifB_A, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(15, 15, 15)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDiferenciaSimet)
                    .addComponent(btnComplementoB))
                .addGap(40, 40, 40))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(235, 235, 235)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUActionPerformed

    private void txtAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAActionPerformed

    private void txtBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBActionPerformed

    private void btnDiferenciaSimetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDiferenciaSimetActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.DifSimetrica();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnDiferenciaSimetActionPerformed

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        // TODO add your handling code here:
        try {
            if (!txtU.getText().equals("") && !txtA.getText().equals("") && !txtB.getText().equals("")) {

                if (!yaContiene(txtU.getText()) && !yaContiene(txtU.getText()) && !yaContiene(txtU.getText())) {
                    String[] U = txtU.getText().split(",");
                    String[] A = txtA.getText().split(",");
                    String[] B = txtB.getText().split(",");

                    fc = new FuncionesCaracteristicas(U, A, B);
                    fc.llenarFU();
                    fc.llenarFB();
                    fc.llenarFA();
                    JOptionPane.showMessageDialog(null, "Se han ingresado los elementos");
                    txtU.setText("");
                    txtA.setText("");
                    txtB.setText("");
                    txtA.setEnabled(false);
                    txtB.setEnabled(false);
                    txtU.setEnabled(true);
                    btnIngresar.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, "Existen elementos repetidos en los conjuntos");
                }

            } else {
                JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar correctamente los campos");
        }

    }//GEN-LAST:event_btnIngresarActionPerformed

    private void btnInterseccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInterseccionActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.Interseccion();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnInterseccionActionPerformed

    private void btnUnionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUnionActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.Union();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnUnionActionPerformed

    private void btnDif_A_BActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDif_A_BActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.RestaA_B();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnDif_A_BActionPerformed

    private void btnComplementoAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComplementoAActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.ComplementoA();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnComplementoAActionPerformed

    private void btnComplementoBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComplementoBActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.ComplementoB();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnComplementoBActionPerformed

    private void btnDifB_AActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDifB_AActionPerformed
        // TODO add your handling code here:
        try {
            int[] fr = fc.RestaB_A();
            String result = fc.resultado(fr);
            txaResultado.setText(result);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Asegúrese de llenar todos los campos");
        }

    }//GEN-LAST:event_btnDifB_AActionPerformed

    public boolean isCodigo(String Codigo) {
        char aux = Codigo.charAt(0);
        int dim = Codigo.length();
        char aux2 = Codigo.charAt(dim - 1);
        if (!Codigo.contains(",,") && aux != ',' && aux2 != ',') {
            return true;
        } else {
            return false;
        }
    }

    private boolean dentroU(String U, String conjunto) {
        boolean resp = true;
        String[] Conjunto = conjunto.split(",");
        for (int i = 0; i < Conjunto.length; i++) {
            if (!U.contains(Conjunto[i])) {
                return false;
            }
        }
        return resp;
    }

    private boolean yaContiene(String conjunto) {
        boolean resp = false;
        String[] Conjunto1 = conjunto.split(",");
        String[] Conjunto2 = conjunto.split(",");
        int cont = 0;
        for (int i = 0; i < Conjunto1.length; i++) {
            cont = 0;
            for (int j = 0; j < Conjunto1.length; j++) {
                if (Conjunto1[i].contains(Conjunto2[j])) {
                    cont++;
                    if (cont > 1) {
                        return true;
                    }
                }
            }
        }
        return resp;
    }
    private void txtUKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if (!Character.isLetter(c)) {
            if (!Character.isDigit(c)) {
                if ((c < ',' || c > ',')) {
                    if (c != evt.VK_BACK_SPACE && c != evt.VK_ENTER) {
                        getToolkit().beep();
                        evt.consume();
                        JOptionPane.showMessageDialog(null, "Error");
                    }

                }
            }
        }

        if (c == evt.VK_ENTER) {
            try {
                if (isCodigo(txtU.getText())) {
                    if (!yaContiene(txtU.getText())) {
                        txtA.setEnabled(true);
                        txtU.setEnabled(false);
                        txtA.requestFocus();
                    } else {
                        JOptionPane.showMessageDialog(null, "Existen elementos repetidos en el conjunto");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error");
                }
            } catch (Exception e) {
            }

        }
    }//GEN-LAST:event_txtUKeyTyped

    private void txtAKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if (!Character.isLetter(c)) {
            if (!Character.isDigit(c)) {
                if ((c < ',' || c > ',')) {
                    if (c != evt.VK_BACK_SPACE && c != evt.VK_ENTER) {
                        getToolkit().beep();
                        evt.consume();
                        JOptionPane.showMessageDialog(null, "Error");
                    }

                }
            }
        }

        if (c == evt.VK_ENTER) {
            try {
                if (isCodigo(txtA.getText())) {
                    if (dentroU(txtU.getText(), txtA.getText())) {
                        if (!yaContiene(txtA.getText())) {
                            txtB.setEnabled(true);
                            txtA.setEnabled(false);
                            txtB.requestFocus();
                        } else {
                            JOptionPane.showMessageDialog(null, "Existen elementos repetidos en el conjunto");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Al menos un elemento del conjunto ingresado NO pertenece al universo");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No puede empezar con ',' o contener comas seguidas ',,'");
                }
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_txtAKeyTyped

    private void txtBKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar();
        if (!Character.isLetter(c)) {
            if (!Character.isDigit(c)) {
                if ((c < ',' || c > ',')) {
                    if (c != evt.VK_BACK_SPACE && c != evt.VK_ENTER) {
                        getToolkit().beep();
                        evt.consume();
                        JOptionPane.showMessageDialog(null, "Error");
                    }

                }
            }
        }

        if (c == evt.VK_ENTER) {
            try {
                if (isCodigo(txtB.getText())) {
                    if (dentroU(txtU.getText(), txtB.getText())) {
                        if (!yaContiene(txtB.getText())) {
                            txtB.setEnabled(false);
                            txtB.requestFocus();
                            btnIngresar.setEnabled(true);
                        } else {
                            JOptionPane.showMessageDialog(null, "Existen elementos repetidos en el conjunto");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Al menos un elemento del conjunto ingresado NO pertenece al universo");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No puede empezar con ',' o contener comas seguidas ',,'");
                }
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_txtBKeyTyped

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(FrmConjuntos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(FrmConjuntos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(FrmConjuntos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(FrmConjuntos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new FrmConjuntos().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnComplementoA;
    private javax.swing.JButton btnComplementoB;
    private javax.swing.JButton btnDifB_A;
    private javax.swing.JButton btnDif_A_B;
    private javax.swing.JButton btnDiferenciaSimet;
    private javax.swing.JButton btnIngresar;
    private javax.swing.JButton btnInterseccion;
    private javax.swing.JButton btnUnion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txaResultado;
    private javax.swing.JTextField txtA;
    private javax.swing.JTextField txtB;
    private javax.swing.JTextField txtU;
    // End of variables declaration//GEN-END:variables
}
